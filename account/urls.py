from django.urls import path, include


app_name = 'account'
# pre-url: /account/
urlpatterns = [
    path('api/', include('account.api.urls'), name='account_api'),
]
