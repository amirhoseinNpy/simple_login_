from django.contrib.auth.models import User
from django.contrib.auth import authenticate
from django.core.exceptions import ObjectDoesNotExist
from rest_framework import generics
from rest_framework.views import APIView
from rest_framework.authtoken.models import Token
from rest_framework.response import Response
from rest_framework import status

from .serializers import SomeTableSerializer, UserSerializer, ChangePasswordSerializer
from ..models import SomeTable


class SomeTableApiView(generics.ListAPIView):
    queryset = SomeTable.objects.all()
    serializer_class = SomeTableSerializer


class UserApiView(generics.CreateAPIView):
    permission_classes = ()
    queryset = User.objects.all()
    serializer_class = UserSerializer


class LoginApiView(APIView):
    permission_classes = ()

    def post(self, request):
        username = request.data.get('username')
        password = request.data.get('password')
        user = authenticate(username=username, password=password)
        if user:
            token, created = Token.objects.get_or_create(user=user)
            return Response(data={'token': token.key})
        else:
            return Response(data={'Error': 'Wrong Credentials'},
                            status=status.HTTP_400_BAD_REQUEST)


class LogoutApiView(APIView):

    def get(self, request):
        Token.objects.get(user=request.user).delete()
        try:
            request.user.auth_token.delete()
        except (AttributeError, ObjectDoesNotExist):
            pass
        return Response(data={'Successful logout': 'Token set expired'},
                        status=status.HTTP_200_OK)


class PasswordResetApiView(generics.UpdateAPIView):
    model = User
    serializer_class = ChangePasswordSerializer

    def get_object(self):
        obj = self.request.user
        return obj

    def update(self, request, *args, **kwargs):
        obj = self.get_object()
        serializer = self.get_serializer(data=request.data)

        if serializer.is_valid():
            if not obj.check_password(serializer.data.get('old_password')):
                return Response({"old_password": "Wrong password."}, status=status.HTTP_400_BAD_REQUEST)
            if serializer.data.get('new_password1') != serializer.data.get('new_password2'):
                return Response({"new_passwords": "Not matched."}, status=status.HTTP_400_BAD_REQUEST)

            obj.set_password(serializer.data.get('new_password1'))
            obj.save()
            return Response({'message': 'Password updated successfully'},
                            status=status.HTTP_200_OK)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
