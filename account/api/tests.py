import requests


class Client(object):
    @staticmethod
    def print_status(response):
        print('Status: ', response.status_code)
        response_data = response.json()
        print(response_data)

    @staticmethod
    def test_deny_permission():
        print('\n*** test_deny_permission (anonymous user) ***')
        response = requests.get('http://127.0.0.1:8000/account/api/some_table/')
        Client.print_status(response)

    @staticmethod
    def test_login():
        print('\n*** test_login (and get token) ***')
        credentials = {'username': 'admin', 'password': 'admin'}
        response = requests.post('http://127.0.0.1:8000/account/api/login/',
                                 data=credentials)
        Client.print_status(response)

    @staticmethod
    def test_token():
        print('\n*** test_token (login) ***')
        credentials = {'username': 'admin', 'password': 'admin'}
        response = requests.post('http://127.0.0.1:8000/account/api/login/',
                                 data=credentials)
        response_data = response.json()
        token = response_data['token']
        headers = {'Authorization': 'Token {}'.format(token)}
        response = requests.get('http://127.0.0.1:8000/account/api/some_table/',
                                headers=headers)
        Client.print_status(response)

    @staticmethod
    def test_expire_token():
        print('\n*** test_expire_token (same log out) ***')
        # get token
        credentials = {'username': 'admin', 'password': 'admin'}
        response = requests.post('http://127.0.0.1:8000/account/api/login/',
                                 data=credentials)
        response_data = response.json()
        token = response_data['token']
        headers = {'Authorization': 'Token {}'.format(token)}
        # log out
        response = requests.get('http://127.0.0.1:8000/account/api/logout/',
                                headers=headers)
        Client.print_status(response)

    @staticmethod
    def test_change_password():
        print('\n*** test_change_password ***')
        # get token
        credentials = {'username': 'admin', 'password': 'admin'}
        response = requests.post('http://127.0.0.1:8000/account/api/login/',
                                 data=credentials)
        response_data = response.json()
        token = response_data['token']
        headers = {'Authorization': 'Token {}'.format(token)}

        # change_password from 'admin' to 'admin'
        credentials = {"old_password": "admin",
                       "new_password1": "admin",
                       "new_password2": "admin"}

        response = requests.put('http://127.0.0.1:8000/account/api/change_password/',
                                 data=credentials, headers=headers)
        Client.print_status(response)


if __name__ == '__main__':
    Client.test_deny_permission()
    Client.test_login()
    Client.test_token()
    Client.test_expire_token()
    Client.test_change_password()
