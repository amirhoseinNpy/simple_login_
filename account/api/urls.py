from django.urls import path
from rest_framework.authtoken.views import obtain_auth_token

from . import views


# pre-url: /account/api/
urlpatterns = [
    path('some_table/', views.SomeTableApiView.as_view(), name='some_table'),
    path('users/', views.UserApiView.as_view(), name='users'),
    path('login/', obtain_auth_token, name='login'),
    # path('login/', views.LoginApiView.as_view(), name='login'),
    path('logout/', views.LogoutApiView.as_view(), name='logout'),
    path('change_password/', views.PasswordResetApiView.as_view(), name='change_password'),
]
