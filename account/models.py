from django.db import models


class SomeTable(models.Model):
    some_field1 = models.CharField(max_length=15)
    some_field2 = models.PositiveIntegerField()

    def __str__(self):
        return self.some_field1
